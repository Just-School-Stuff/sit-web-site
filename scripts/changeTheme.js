/*
#################################################################################################################################
# Copyright (c) 2019 MUHAMMED SAID BILGEHAN
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. All advertising materials mentioning features or use of this software
#    must take permission from MUHAMMED SAID BILGEHAN and must display the
#	   following acknowledgement:
#    This product includes software developed by the MUHAMMED SAID BILGEHAN.
# 4. Neither the name of the MUHAMMED SAID BILGEHAN nor the
#    names of its contributors may be used to endorse or promote products
#    derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY MUHAMMED SAID BILGEHAN ''AS IS'' AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL MUHAMMED SAID BILGEHAN BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBLITY OF SUCH DAMAGE.

#################################################################################################################################
*/

/* Change theme with button */
/*
  Some Usefull links;
    https://davidwalsh.name/css-variables-javascript
*/
function themeChange(){
  if( getComputedStyle(document.documentElement).getPropertyValue("--textC") == "white"){
    /*For White Theme*/
    document.documentElement.style.setProperty("--textC", "black");
    document.documentElement.style.setProperty("--backgroundC", "white");
    document.documentElement.style.setProperty("--shadowC", "#4444");
    document.documentElement.style.setProperty("--navMenuC", "#fff");
    document.documentElement.style.setProperty("--buttonC", "#333 1.0");
    document.documentElement.style.setProperty("--other1C", "grey");
    document.documentElement.style.setProperty("--other2C", "#b3b3b3");

    document.getElementById("themeIcon").style.filter = "invert(0)";
    document.getElementById("themeIcon").style.webkitFilter = "invert(0)";

    document.getElementById("cvDefaultImage").style.filter = "invert(0)";
    document.getElementById("cvDefaultImage").style.webkitFilter = "invert(0)";

  }
  else {
    /*For Black Theme*/
    document.documentElement.style.setProperty("--textC", "white");
    document.documentElement.style.setProperty("--backgroundC", "#222");
    document.documentElement.style.setProperty("--navMenuC", "#333");
    document.documentElement.style.setProperty("--shadowC", "#252525");
    document.documentElement.style.setProperty("--buttonC", "#333 1.0");
    document.documentElement.style.setProperty("--other1C", "#777");
    document.documentElement.style.setProperty("--other2C", "#777");

    document.getElementById("themeIcon").style.filter = "invert(100%)";
    document.getElementById("themeIcon").style.webkitFilter = "invert(100%)";

    document.getElementById("cvDefaultImage").style.filter = "invert(100%)";
    document.getElementById("cvDefaultImage").style.webkitFilter = "invert(100%)";

  }
}
