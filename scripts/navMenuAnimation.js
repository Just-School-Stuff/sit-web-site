/*
#################################################################################################################################
# Copyright (c) 2019 MUHAMMED SAID BILGEHAN
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. All advertising materials mentioning features or use of this software
#    must take permission from MUHAMMED SAID BILGEHAN and must display the
#	   following acknowledgement:
#    This product includes software developed by the MUHAMMED SAID BILGEHAN.
# 4. Neither the name of the MUHAMMED SAID BILGEHAN nor the
#    names of its contributors may be used to endorse or promote products
#    derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY MUHAMMED SAID BILGEHAN ''AS IS'' AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL MUHAMMED SAID BILGEHAN BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBLITY OF SUCH DAMAGE.

#################################################################################################################################
*/
/* Set the width of the side navigation to 250px */
function openMenu() {
  document.getElementById("acordMenu").style.display = "flex";
  document.getElementById("acordMenu").style.height = "250px";
  document.getElementById("acordMenu").style.boxShadow = "0pt 0pt 17pt 5pt var(--shadowC)";
  document.getElementById("acordMenu").style.mozBoxShadow = "0pt 0pt 17pt 5pt var(--shadowC)";
  document.getElementById("acordMenu").style.webkitBoxShadow = "0pt 0pt 17pt 5pt var(--shadowC)";
}

/* Set the width of the side navigation to 0 */
function closeMenu() {
  document.getElementById("acordMenu").style.display = "none";
  document.getElementById("acordMenu").style.height = "0px";
  document.getElementById("acordMenu").style.boxShadow = "0px 0px 0px white";
  document.getElementById("acordMenu").style.moxBoxShadow = "0px 0px 0px white";
  document.getElementById("acordMenu").style.webkitBoxShadow = "0px 0px 0px white";
}

function acordMenuControl(){
  if ( document.getElementById("acordMenu").style.width == 0 ) {
    openMenu();
  }
  else {
    closeMenu();
  }
}
